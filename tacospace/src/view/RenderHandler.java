package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import model.Game;
import model.ents.Entity;
import model.ents.Sun;
import model.ents.Unit;
import model.gui.GUIElement;
import model.input.SelectionBox;
import model.spatial.MapGrid;
import model.GameHandler;

public class RenderHandler {

	BufferedImage backgroundImage;

	public RenderHandler() {
		File backgroundImageFile;
		try {
			backgroundImageFile = new File(getClass().getResource("/resource/image/skybox_14.png").toURI());
			backgroundImage = ImageIO.read(backgroundImageFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void render(Graphics2D surface, int offsetX, int offsetY) {
		/*
		 * ENTITY AND GAME RENDERING
		 */

		LinkedList<Entity> ents = Game.gameHandler.getEntities();
		if (!(Game.mainMenu())) {

			surface.drawImage(backgroundImage, 0 - backgroundImage.getWidth() / 2 + Game.WIDTH / 2 + offsetX / 10,
					0 - backgroundImage.getHeight() / 2 + Game.HEIGHT / 2 + offsetY / 10, null);

			// Map borders
			surface.setColor(Color.green);
			// top left to top right
			surface.drawLine(0 + offsetX, 0 + offsetY, Game.MAX_MAP_WIDTH + offsetX, 0 + offsetY);
			// top left to bottom left
			surface.drawLine(0 + offsetX, 0 + offsetY, 0 + offsetX, Game.MAX_MAP_HEIGHT + offsetY);
			// top right to bottom right
			surface.drawLine(Game.MAX_MAP_WIDTH + offsetX, 0 + offsetY, Game.MAX_MAP_WIDTH + offsetX,
					Game.MAX_MAP_HEIGHT + offsetY);
			// bottom left to bottom right
			surface.drawLine(0 + offsetX, Game.MAX_MAP_HEIGHT + offsetY, Game.MAX_MAP_WIDTH + offsetX,
					Game.MAX_MAP_HEIGHT + offsetY);

			// center, used for debug, kinda
			// surface.drawRect(Game.MAX_MAP_WIDTH / 2 - 2 + offsetX, Game.MAX_MAP_HEIGHT /
			// 2 - 2 + offsetY, 4, 4);

			// grid lines (originally for debug but I kinda like them?)
			for (int i = 1; i < Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE; i++) {
				surface.setColor(new Color(0, 255, 0, 10));
				surface.drawLine(0 + offsetX, i * 32 + offsetY, Game.MAX_MAP_WIDTH + offsetX, i * 32 + offsetY);
				surface.drawLine(i * 32 + offsetX, 0 + offsetY, i * 32 + offsetX, Game.MAX_MAP_HEIGHT + offsetY);
			}

			for (int i = 0; i < ents.size(); i++) {
				Entity ent = ents.get(i);
				switch (ent.getClassName()) {

				case "entity_sun":
					Sun sun = (Sun) ent;
					int fps = Game.getFPS();

					// implement with interface ownable?
					if (!(sun.getTeam() == null)) {
						surface.setColor(sun.getTeam().getColor());
						sun.setBeat(true);
					} else {
						surface.setColor(Color.LIGHT_GRAY);
						sun.setBeat(false);
					}

					// saving color for the beat transparency modulation, will be used again later
					Color teamColor = surface.getColor();

					/*
					 * BEAT RENDERING rendered before planet body to have it appear behind the body
					 */
					if (sun.getBeatRadius() < sun.getBeatRadiusMax() && sun.isBeat()) {
						surface.setColor(new Color(teamColor.getRed(), teamColor.getGreen(), teamColor.getBlue(),
								(int) sun.getBeatTransparency()));
						surface.setStroke(new BasicStroke(10F));
						// doing / 2 because of course "beatRadius" is actually the diameter... too bad!
						surface.drawOval((int) (sun.getX() - sun.getBeatRadius() / 2) + offsetX,
								(int) (sun.getY() - sun.getBeatRadius() / 2) + offsetY, (int) sun.getBeatRadius(),
								(int) sun.getBeatRadius());

						// advance beat radius and transparency animation
						// math will result in one animation cycle per beat
						if (Game.gameRunning()) {
							sun.setBeatRadius(sun.getBeatRadius()
									+ ((float) sun.getBeatRadiusMax() / (fps / sun.getBeatsPerSecond())));
							sun.setBeatTransparency(sun.getBeatTransparency()
									- ((sun.getBeatTransparency() - ((float) 255 / (fps / sun.getBeatsPerSecond())) > 0)
											? ((float) 255 / (fps / sun.getBeatsPerSecond()))
											: 0));
						}
					}

					// reset stroke thickness after using it for the beat circle
					surface.setStroke(new BasicStroke(1));

					/*
					 * PLANET RENDERING
					 */
					surface.setColor(new Color(teamColor.getRed(), teamColor.getGreen(), teamColor.getBlue(), 255));
					surface.fillOval(sun.getX() - 75 + offsetX, sun.getY() - 75 + offsetY, 150, 150);
					surface.setColor(Color.green);
					if (sun.getHealth() < sun.getMaxHealth()) {

						surface.setColor(Color.black);
						surface.fillRect(sun.getX() + offsetX - 12, sun.getY() + offsetY - 52, 24, 104);
						if (sun.getTeam() == null)
							surface.setColor(Color.LIGHT_GRAY);
						else
							surface.setColor(sun.getTeam().getColor());
						surface.fillRect(sun.getX() + offsetX - 10, sun.getY() + offsetY - 50, 20,
								sun.getHealth() * (100 / sun.getMaxHealth()));

						// surface.setColor(Color.green);
						// surface.drawString("" + sun.getHealth(), sun.getX() + offsetX, sun.getY() +
						// offsetY);
					}

					break;

				case "entity_unit":
					Unit unit = (Unit) ent;

					// halo for when units are selected by the player
					if (unit.isSelected()) {
						surface.setColor(new Color(255, 255, 255, 255));
						surface.fillRect(unit.getX() - 6 + offsetX, unit.getY() - 6 + offsetY, 12, 12);
					}

					surface.setColor(new Color(0, 0, 0, 255));
					surface.fillRect(unit.getX() - 5 + offsetX, unit.getY() - 5 + offsetY, 10, 10);
					if (!(unit.getTeam() == null)) {
						surface.setColor(unit.getTeam().getColor());
					} else {
						surface.setColor(Color.LIGHT_GRAY);
					}
					surface.fillRect(unit.getX() - 3 + offsetX, unit.getY() - 3 + offsetY, 6, 6);

					/*
					 * MOVEMENT DEBUG RENDERING
					 */
					// surface.setFont(new Font("Electrolize", Font.PLAIN, 13));
					// surface.setColor(Color.GREEN);
					// surface.drawString(this.cell.getIndexX() + " " + this.cell.getIndexY(),
					// this.getX() + 10 + offsetX, this.getY() + 10 + offsetY);
					// surface.drawString("x: " + this.getX(), this.getX() + 10 + offsetX,
					// this.getY() + 10 + offsetY);
					// surface.drawString("y: " + this.getY(), this.getX() + 10 + offsetX,
					// this.getY() + 20 + offsetY);
					//
					// surface.drawString("x: " + (int) this.moveTarget.getX(), (int)
					// this.moveTarget.getX() + 10 + offsetX, (int) this.moveTarget.getY() + - 20 +
					// offsetY);
					// surface.drawString("y: " + (int) this.moveTarget.getY(), (int)
					// this.moveTarget.getX() + 10 + offsetX, (int) this.moveTarget.getY() + - 10 +
					// offsetY);
					// surface.drawLine(this.getX() + offsetX, this.getY() + offsetY, (int)
					// moveTarget.getX() + offsetX, (int) moveTarget.getY() + offsetY);
					//
					// surface.setColor(Color.red);
					// surface.drawLine(this.getX() + offsetX, this.getY() + offsetY, (int)
					// moveGoal.getX() + offsetX, (int) moveGoal.getY() + offsetY);
					// surface.setColor(Color.blue);
					// surface.drawLine((int) this.moveTarget.getX(), (int) this.moveTarget.getY(),
					// (int) this.moveTarget.getX(), (int) this.moveTarget.getY() - 20 );

					break;
				}
			}
		}

		/*
		 * SELECTION BOX RENDERING
		 */

		if (SelectionBox.isSelecting()) {
			surface.setColor(new Color(20, 255, 20));

			// have to do this crap because drawRect goes solid color with negative width or
			// height
			if (SelectionBox.getEndX() < SelectionBox.getX() && SelectionBox.getEndY() < SelectionBox.getY()) {
				surface.drawRect(SelectionBox.getEndX(), SelectionBox.getEndY(),
						SelectionBox.getX() - SelectionBox.getEndX(), SelectionBox.getY() - SelectionBox.getEndY());
				surface.setColor(new Color(20, 255, 20, 30));
				surface.fillRect(SelectionBox.getEndX(), SelectionBox.getEndY(),
						SelectionBox.getX() - SelectionBox.getEndX(), SelectionBox.getY() - SelectionBox.getEndY());
			} else if (SelectionBox.getEndX() < SelectionBox.getX() && SelectionBox.getEndY() > SelectionBox.getY()) {
				surface.drawRect(SelectionBox.getEndX(), SelectionBox.getY(),
						SelectionBox.getX() - SelectionBox.getEndX(), SelectionBox.getEndY() - SelectionBox.getY());
				surface.setColor(new Color(20, 255, 20, 30));
				surface.fillRect(SelectionBox.getEndX(), SelectionBox.getY(),
						SelectionBox.getX() - SelectionBox.getEndX(), SelectionBox.getEndY() - SelectionBox.getY());
			} else if (SelectionBox.getEndX() > SelectionBox.getX() && SelectionBox.getEndY() < SelectionBox.getY()) {
				surface.drawRect(SelectionBox.getX(), SelectionBox.getEndY(),
						SelectionBox.getEndX() - SelectionBox.getX(), SelectionBox.getY() - SelectionBox.getEndY());
				surface.setColor(new Color(20, 255, 20, 30));
				surface.fillRect(SelectionBox.getX(), SelectionBox.getEndY(),
						SelectionBox.getEndX() - SelectionBox.getX(), SelectionBox.getY() - SelectionBox.getEndY());
			} else {
				surface.drawRect(SelectionBox.getX(), SelectionBox.getY(), SelectionBox.getEndX() - SelectionBox.getX(),
						SelectionBox.getEndY() - SelectionBox.getY());
				surface.setColor(new Color(20, 255, 20, 30));
				surface.fillRect(SelectionBox.getX(), SelectionBox.getY(), SelectionBox.getEndX() - SelectionBox.getX(),
						SelectionBox.getEndY() - SelectionBox.getY());
			}
		}

		/*
		 * GUI RENDERING
		 */

		surface.setFont(new Font("Electrolize", Font.PLAIN, 35));

		LinkedList<GUIElement> guielements = Game.guiHandler.getElements();

		switch (Game.getGameState()) {

		case Game.GAME_STATE_MAIN_MENU:
			surface.drawImage(backgroundImage, 0 - backgroundImage.getWidth() / 2 + Game.WIDTH / 2,
					0 - backgroundImage.getHeight() / 2 + Game.HEIGHT / 2, null);
			surface.setFont(new Font("Electrolize", Font.PLAIN, 20));
			surface.setColor(Color.green);
			int textHeight = surface.getFontMetrics().getAscent() + surface.getFontMetrics().getDescent();
			surface.drawString("� MilkyWae Inc.", 15, Game.HEIGHT - textHeight - 30);
			// surface.drawLine(0, Game.HEIGHT / 2, Game.WIDTH, Game.HEIGHT / 2);
			surface.setFont(new Font("Electrolize", Font.PLAIN, 110));
			int textWidth = surface.getFontMetrics().stringWidth("SPACETACOS");
			surface.drawString("SPACETACOS", Game.WIDTH / 2 - textWidth / 2, Game.HEIGHT / 2 - 20 - 80 - 40);
			break;

		case Game.GAME_STATE_MAP_SELECT:

			break;

		case Game.GAME_STATE_GAME_MENU:
			surface.setColor(Color.green);
			surface.drawString("GAME PAUSED", Game.WIDTH / 2 - 200, Game.HEIGHT / 2 - 20 - 80 - 20 - 20);
			surface.setColor(new Color(30, 30, 30, 150));
			surface.fillRect(Game.WIDTH / 2 - 200 - 20, Game.HEIGHT / 2 - 20 - 80 - 20, 440, 240);
			break;

		case Game.GAME_STATE_WIN:
			surface.setColor(Color.green);
			int textWidthGameWin = surface.getFontMetrics().stringWidth("GAME WON");
			surface.drawString("GAME WON", Game.WIDTH / 2 - textWidthGameWin / 2, Game.HEIGHT / 2);
			break;

		case Game.GAME_STATE_LOSS:
			surface.setColor(Color.green);
			int textWidthGameLoss = surface.getFontMetrics().stringWidth("GAME OVER");
			surface.drawString("GAME OVER", Game.WIDTH / 2 - textWidthGameLoss / 2, Game.HEIGHT / 2);
			break;

		default:
			break;

		}

		/*
		 * GUI ELEMENT RENDERING
		 */

		surface.setFont(new Font("Electrolize", Font.PLAIN, 35));
		for (int i = 0; i < guielements.size(); i++) {
			GUIElement element = guielements.get(i);
			if (Game.getGameState() == element.getGameState()) {
				surface.setClip(element.getX(), element.getY(), element.getWidth() + 1, element.getHeight() + 1);
				surface.setColor(new Color(50, 200, 50, 100));
				if (element.isColoured())
					surface.setColor(new Color(element.getColor().getRed(), element.getColor().getGreen(),
							element.getColor().getBlue(), 220));
				if (element.isHovered()) {
					surface.setColor(new Color(0, 255, 0, 120));
					if (element.isColoured())
						surface.setColor(new Color(element.getHovercolor().getRed(), element.getHovercolor().getGreen(),
								element.getHovercolor().getBlue(), 255));
				}
				surface.fillRect(element.getX(), element.getY(), element.getWidth(), element.getHeight());

				surface.setColor(new Color(50, 255, 50));
				if (element.isColoured())
					surface.setColor(new Color(element.getColor().getRed(), element.getColor().getGreen(),
							element.getColor().getBlue(), 255));
				surface.drawRect(element.getX(), element.getY(), element.getWidth(), element.getHeight());
				int textWidth = surface.getFontMetrics().stringWidth(element.getTitle());
				int textHeight = surface.getFontMetrics().getAscent(); // height from baseline, ignoring descending
																		// height for predictable outcome
				surface.drawString(element.getTitle(), element.getX() + (element.getWidth() / 2) - (textWidth / 2),
						element.getY() + (element.getHeight() / 2) + (textHeight / 2));
				if (element.isUnderlined())
					surface.drawLine(element.getX() + (element.getWidth() / 2) - (textWidth / 2),
							element.getY() + (element.getHeight() / 2) + (textHeight / 2) + 2,
							element.getX() + (element.getWidth() / 2) - (textWidth / 2) + textWidth,
							element.getY() + (element.getHeight() / 2) + (textHeight / 2) + 2);
			}
		}

	}

}
