package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.swing.JFrame;

import model.Game;
import model.GameHandler;
import model.ents.*;

public class Window {

	private DecimalFormat df;
	private static Camera camera;

	public Window(int width, int height, String title, Game game, Camera camera) {
		
		Window.camera = camera;
		Game.gameHandler.addObject(camera);
		
		JFrame frame = new JFrame(title);
		// we really don't want the window resized ok. No time for pesky rescale
		frame.setPreferredSize(new Dimension(width, height));
		frame.setMaximumSize(new Dimension(width, height));
		frame.setMinimumSize(new Dimension(width, height));
		frame.setResizable(false);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.add(game);
		frame.setVisible(true);
		frame.setBackground(Color.black);

		df = new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.CEILING);

		game.start();
	}
	
	public static Camera getCamera() {
		return Window.camera;
	}

	public void renderGame(Game game, RenderHandler renderHandler) {
		BufferStrategy bufferStrat = game.getBufferStrategy();
		if (bufferStrat == null) {
			// triple buffer strategy
			game.createBufferStrategy(3);
			return;
		}
		// Surface actions
		Graphics2D surface = (Graphics2D) bufferStrat.getDrawGraphics();
		surface.setColor(Color.black);
		surface.fillRect(0, 0, game.getWidth(), game.getHeight());

		// Render only on screen
		surface.setClip(0, 0, game.getWidth(), game.getHeight());
		
		// surface.setColor(Color.GREEN);
		// surface.fillRect(50, 50, 50, 50);
		
		surface.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		surface.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

		surface.setFont(new Font("Electrolize", Font.PLAIN, 25));

		renderHandler.render(surface, camera.getOffsetX(), camera.getOffsetY());
		
		// constant FPS and TPS numbers in the top left corner
        surface.setFont(new Font("Electrolize", Font.PLAIN, 13));
        surface.setColor(new Color(50, 50, 50, 150));
        surface.fillRect(0, 0, 75, 60);
        surface.setColor(Color.green);
        surface.drawString("FPS: " + Game.getFPS(), 0, 10);
        surface.drawString("FT: " + df.format(Game.getFrameTime()), 0, 25);
        surface.drawString("TPS: " + Game.getTPS(), 0, 40);
        surface.drawString("ENTS: " + GameHandler.numEntities, 0, 55);
		
		surface.dispose();
		bufferStrat.show();
	}
	

}
