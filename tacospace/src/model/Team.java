package model;

import java.awt.Color;

public class Team {
	/*
	 * mostly used for team specific color to represent their owner
	 * has a String name and Color teamColor for easy usage
	 */
	private Color teamColor;
	private String name;
	
	public Team(String teamName, Color teamColor) {
		this.name = teamName;
		this.teamColor = teamColor;
		Game.registerTeam(this);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Color getColor() {
		return this.teamColor;
	}
	
}
