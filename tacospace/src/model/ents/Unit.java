package model.ents;

import java.util.LinkedList;

import model.Game;
import model.MoveTarget;
import model.Team;
import model.spatial.Cell;
import model.spatial.MapGrid;
import model.util.Point;
import model.util.Util;

public class Unit extends Entity {
	
	// movement & attack vars
	private MoveTarget moveTarget;
	private MoveTarget moveGoal;
	private double unitVectorX, unitVectorY, magnitude;
	private double maxVelocityComponent = 3;
	private boolean moved, attacked = false;
	
	private Cell cell;
	
	// possession of the unit
	private Team team;
	private boolean selected;
	
	// used with the Player's selection box
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public Unit(double x, double y, Team team) {
		super(x, y);
		this.cell = MapGrid.getCell(this.getX() / MapGrid.MAP_GRIDSIZE, this.getY() / MapGrid.MAP_GRIDSIZE);
		this.cell.add(this);
		this.team = team;
		this.moveGoal = new MoveTarget(new Point(x, y));
		this.moveTarget = new MoveTarget(new Point(x, y));
	}
	public Unit(double x, double y) {
		super(x, y);
		this.cell = MapGrid.getCell(this.getX() / MapGrid.MAP_GRIDSIZE, this.getY() / MapGrid.MAP_GRIDSIZE);
		this.cell.add(this);
		this.team = null;
		this.moveGoal = new MoveTarget(new Point(x, y));
		this.moveTarget = new MoveTarget(new Point(x, y));
	}
	
	public void setMoveGoal(Point point) {
		this.moveGoal.setPos(point);
	}
	
	private void moveTo(Point point) {
		this.moveTarget.setPos(point);
		this.magnitude = new Point(this.getPosX(), this.getPosY()).getDistance(this.moveGoal.getPos());
		this.unitVectorX = ( this.moveGoal.getX() - this.getPosX() ) / this.magnitude;
		this.unitVectorY = ( this.moveGoal.getY() - this.getPosY() ) / magnitude;
	}

	public void tick() {
		// don't tick while game is paused
		if(Game.gameRunning()) {
			
			// update grid pos for spatial partitioning
			updateGridPos();
			
			// check collisions for unit avoidance, attacking ang homing
			checkCollisions();
			
			// move the unit towards it's move target/goal
			move();
		}
	}
	
	private void move() {
		/*
		 * linear movement for predictable travel time of units
		 */
		
		// make the unit stop once it's "close enough"
		// for some reason necessary when combined with the collision etc
		if(this.moveGoal.getPos().getDistance(new Point(this.getX(), this.getY())) < 5 && this.moved) {
			this.moveTarget.setPos(new Point(this.getPosX(), this.getPosY()));
		}
		
		// unitvector (direction vector) multiplied by a predefined constant for the velocity to move
		this.setVelX(this.unitVectorX * this.maxVelocityComponent);
		this.setVelY(this.unitVectorY * this.maxVelocityComponent);
		
		// move by the scaled unitVector every tick, until it is larger than the distance to the moveTarget
		// then just move the rest of that distance. Next tick will stop the unit
		this.x += ( Math.abs(this.unitVectorX) > Math.abs((this.moveTarget.getX() - this.getPosX())) ) ? this.moveTarget.getX() - this.getPosX() : this.velX;
		this.y += ( Math.abs(this.unitVectorY) > Math.abs((this.moveTarget.getY() - this.getPosY())) ) ? this.moveTarget.getY() - this.getPosY() : this.velY;
		
		// stuck workaround
		this.moved = true;
	}
	
	private void checkCollisions() {
		/*
		 * proximity based avoidance, basically simple "collision"
		 * it kinda works but seems very yanky
		 */
		
		// if we dodged last tick, try to get back on track
		if(!this.moveGoal.getPos().isSamePosition(this.moveTarget.getPos())) {
			this.moveTo(this.moveGoal.getPos());
		}
		
		// check all close cells for entities
		LinkedList<Entity> ents = MapGrid.getNeighbours(this.cell.getIndexX(), this.cell.getIndexY());
		Point thisPos = new Point(this.getPosX(), this.getPosY());
		for (int i = 0; i < ents.size(); i++) {
			Entity ent = ents.get(i);
			
			// handle unit to unit collision
			if (ent.getClassName().equals("entity_unit")) {
				Unit unit = (Unit) ent;
				
				if (unit == this) // this is me, I cannot be too close to myself, onto the next unit
					continue;
				
				Point unitPos = new Point(unit.getPosX(), unit.getPosY());
				double unitDistance = unitPos.getDistance(thisPos);
				if ( unitDistance < 100 ) { // are we too close to this one unit?
					if (this.getTeam() == unit.getTeam()) {
						if (unitDistance < 15) {
							if (thisPos.getDistance(this.moveGoal.getPos()) < 5) {
								// we're close enough to the goal at the time of "collision"
								this.setMoveGoal(new Point( -( unitPos.getPosX() - thisPos.getPosX() ) + thisPos.getPosX(), -( unitPos.getPosY() - thisPos.getPosY() ) + thisPos.getPosY() ));
							} else {
								if (this.moved) 
									this.moveTo(new Point( -( unitPos.getPosX() - thisPos.getPosX() ) + thisPos.getPosX(), -( unitPos.getPosY() - thisPos.getPosY() ) + thisPos.getPosY() ));
								else {
									// anti stuck workaround (I hope)
									double random = Util.random(-2, 2);
									this.moveTo(new Point( this.getX() + random, this.getY() + random ));
								}
							}
						}
					// enemy units nullify each other
					} else if (!this.attacked && !unit.attacked){
						if (unitDistance < 10) {
							this.attacked = true;
							unit.attacked = true;
							Game.gameHandler.killEntity(this);
							Game.gameHandler.killEntity(unit);
						}
						// hominh of units
						if (this.moveGoal.getPos().getDistance(this.moveGoal.getPos()) < 15)
							this.setMoveGoal(new Point(unit.getPosX(), unit.getPosY()));
						else
							this.moveTo(new Point(unit.getPosX(), unit.getPosY()));
						
						if (unit.moveGoal.getPos().getDistance(unit.moveGoal.getPos()) < 15)
							unit.setMoveGoal(new Point(this.getPosX(), this.getPosY()));
						else
							unit.moveTo(new Point(this.getPosX(), this.getPosY()));
					}
				}
				
			// handle unit to sun collision
			} else if (ent.getClassName().equals("entity_sun")) {
				Sun sun = (Sun) ent;
				if (!(sun.getTeam() == this.getTeam())) {
					Point sunPos = new Point(sun.getPosX(), sun.getPosY());
					if (thisPos.getDistance(sunPos) < 75 && !this.attacked) {
						this.attacked = true;
						this.setMoveGoal(new Point(this.getPosX(), this.getPosY()));
						sun.attack(this);
						Game.gameHandler.killEntity(this);
					}
				}
			}
		}
	}
	
	private void updateGridPos() {
		/*
		 * update cell position in grid
		 */
		
		int indexX = this.getX() / MapGrid.MAP_GRIDSIZE;
		int indexY = this.getY() / MapGrid.MAP_GRIDSIZE;
		
		if(indexX != this.cell.getIndexX() || indexY != this.cell.getIndexY()) {
			this.cell.remove(this);
			this.cell = new Cell(this.getX(), this.getY());
			this.cell.add(this);
		}
	}
	
	public String getClassName() {
		return "entity_unit";
	}
	
	// use an interface ownable???
	public Team getTeam() {
		return this.team;
	}

}
