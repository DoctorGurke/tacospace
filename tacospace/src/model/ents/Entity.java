package model.ents;

import model.Game;

public abstract class Entity {
	
	protected double x, y, velX, velY, accX, accY;
	protected String className;
	public abstract void tick();
	//public abstract void render(Graphics2D surface, int offsetX, int offsetY);
	public abstract String getClassName();
	
	public Entity(double x, double y) {
		this.x = (x > 0 && x < Game.MAX_MAP_WIDTH - 1) ? x : (x < 0) ? 0 : Game.MAX_MAP_WIDTH - 1;
		this.y = (y > 0 && y < Game.MAX_MAP_HEIGHT - 1) ? y : (y < 0) ? 0 : Game.MAX_MAP_HEIGHT - 1;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	// on screen, used for rendering
	public int getX() {
		return (int) this.x;
	}
	
	public int getY() {
		return (int) this.y;
	}
	
	// actual position
	public double getPosX() {
		return this.x;
	}
	
	public double getPosY() {
		return this.y;
	}
	
	public void setVelX(double velX) {
		this.velX = velX;
	}
	
	public void setVelY(double velY) {
		this.velY = velY;
	}
	
	public double getVelX() {
		return this.velX;
	}
	
	public double getVelY() {
		return this.velY;
	}
	
	// if movement is acceleration based
	// originally just made for the camera
	
	public void setAccX(double accX) {
		this.accX = accX;
	}
	
	public void setAccY(double accY) {
		this.accY = accY;
	}
	
	public double getAccX() {
		return this.accX;
	}
	
	public double getAccY() {
		return this.accY;
	}
}
