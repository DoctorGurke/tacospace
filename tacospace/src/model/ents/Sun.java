package model.ents;

import model.Game;
import model.Team;
import model.WinCondition;
import model.spatial.Cell;
import model.spatial.MapGrid;
import model.util.Point;
import model.util.Util;

public class Sun extends Entity {
	
	private String className = "entity_sun";
	
	private Team team;
	
	// beat vars. Used for unit production
	private long beatTimer;
	private int beatRadiusMax = 350;
	private double beatRadius = 0;
	private double beatTransparency = 255;
	private boolean beat = false;
	private double beatsPerSecond = 1;
	private int unitsPerBeat = 1;
	
	private int health = 25;
	private int maxHealth= 25;

	// int since suns don't move so we only care about render location, no precision needed
	public Sun(int x, int y) {
		super(x, y);
		// add sun entity to surrounding cells
		// since suns don't move, this is enough
		for (int indx = -3; indx <= 3; indx++) {
			for (int indy = -3; indy < 3; indy++) {
				Cell cell = MapGrid.getCell(this.getX() / MapGrid.MAP_GRIDSIZE + indx, this.getY() / MapGrid.MAP_GRIDSIZE + indy);
				cell.add(this);
			}
		}
	}
	
	public Sun(int x, int y, Team team) {
		super(x, y);
		this.team = team;
		beatTimer = System.currentTimeMillis();
		// add sun entity to surrounding cells
		// since suns don't move, this is enough
		for (int indx = -3; indx <= 3; indx++) {
			for (int indy = -3; indy < 3; indy++) {
				Cell cell = MapGrid.getCell(this.getX() / MapGrid.MAP_GRIDSIZE + indx, this.getY() / MapGrid.MAP_GRIDSIZE + indy);
				cell.add(this);
			}
		}
	}

	public void tick() {
		if (Game.getGameState() == Game.GAME_STATE_RUNNING) {
			// unit production beat, 1000 ms = 1 s
			
			if (System.currentTimeMillis() - beatTimer > ( 1000 / getBeatsPerSecond() ) && isBeat() ) {
				generateUnit(unitsPerBeat);
			} 
		} else {
			beatTimer = System.currentTimeMillis();
		}
	}
	
	public void generateUnit(int units) {
		// beat animation reset
		beatTimer += ( 1000 / getBeatsPerSecond() );
		setBeatRadius(0);
		setBeatTransparency(255);
		
		// generating numbers for default orbit move target
		// starting orbit is split into n segments of unitsPerBeat to spread them out sudo evenly
		double randomAngle = Util.random(0, 360 / (unitsPerBeat));
		int randomRadius = Util.random(140, 160);
		for (int i = 0; i < units; i++) {
			
			Unit newUnit = new Unit(this.getPosX(), this.getPosY(), this.getTeam());
			Game.gameHandler.addObject(newUnit);
			
			// polar conversion to get moveTo location 
			double randX = randomRadius * Math.cos(Math.toRadians(randomAngle + (360 / (unitsPerBeat)) * i));
			double randY = randomRadius * Math.sin(Math.toRadians(randomAngle + (360 / (unitsPerBeat)) * i));
			
			newUnit.setMoveGoal(new Point(randX + this.getPosX(), randY + this.getPosY()));
		}
	}
	
	public void attack(Unit unit) {
		if (this.getHealth() > 1) {
			this.setHealth(this.getHealth() - 1);
		}else if (this.team == null) {
			this.setHealth(this.maxHealth);
			this.setTeam(unit.getTeam());
			WinCondition.updateNode();
		} else {
			this.setHealth(this.maxHealth);
			this.setTeam(null);
			WinCondition.updateNode();
		}
	}
	
	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	// ownership stuff since suns can switch owner
	public void setTeam(Team team) {
		if (this.team == null)
			beatTimer = System.currentTimeMillis();
		this.team = team;
		this.setHealth(this.maxHealth);
		WinCondition.updateNode();
	}
	public Team getTeam() {
		return this.team;
	}

	public String getClassName() {
		return this.className;
	}

	public double getBeatRadius() {
		return beatRadius;
	}

	public void setBeatRadius(double d) {
		this.beatRadius = d;
	}

	public int getBeatRadiusMax() {
		return beatRadiusMax;
	}

	public void setBeatRadiusMax(int beatRadiusMax) {
		this.beatRadiusMax = beatRadiusMax;
	}

	public boolean isBeat() {
		return beat;
	}

	public void setBeat(boolean beat) {
		this.beat = beat;
	}

	public double getBeatTransparency() {
		return beatTransparency;
	}

	public void setBeatTransparency(double d) {
		this.beatTransparency = d;
	}

	public double getBeatsPerSecond() {
		return beatsPerSecond;
	}

	public void setBeatsPerSecond(double beatsPerSecond) {
		this.beatsPerSecond = beatsPerSecond;
	}

	public int getMaxHealth() {
		return this.maxHealth;
	}
	
	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

}
