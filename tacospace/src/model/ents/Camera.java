package model.ents;

import model.Game;

public class Camera extends Entity {
	
	private double maxVelocityComponent = 10;
	private double frictionCoefficient = 0.9;
	
	public Camera(int x, int y) {
		super(x, y);
	}
	
	public void tick() {
		if (Game.getGameState() == Game.GAME_STATE_RUNNING || Game.getGameState() == Game.GAME_STATE_PAUSED || Game.getGameState() == Game.GAME_STATE_LOSS || Game.getGameState() == Game.GAME_STATE_WIN) {
			/*
			 * Simple acceleration and friction based movement to allow for smooth camera movement
			 * (is really cool)
			 */
			
			double accelX = this.getAccX();
			double accelY = this.getAccY();
			
			// apply acceleration depending on axis
			this.velX += ((accelX != 0) ? (accelX > 0) ? (this.velX < this.maxVelocityComponent) ? accelX : 0 : (accelX < 0) ? (this.velX > -this.maxVelocityComponent) ? accelX : 0 : 0 : 0);
			this.velY += ((accelY != 0) ? (accelY > 0) ? (this.velY < this.maxVelocityComponent) ? accelY : 0 : (accelY < 0) ? (this.velY > -this.maxVelocityComponent) ? accelY : 0 : 0 : 0); 
			
			// reduce velocity steadily by a friction factor
			this.velX *= this.frictionCoefficient;
			this.velY *= this.frictionCoefficient;
			
			// move by velocity
			this.x += this.velX;
			this.y += this.velY;
		}
	}
	
	public String getClassName() {
		return "func_camera";
	}
	
	public int getOffsetX() {
		return this.getX() - Game.WIDTH / 2;
	}
	
	public int getOffsetY() {
		return this.getY() - Game.HEIGHT / 2;
	}
	
}
