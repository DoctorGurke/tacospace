package model;

import java.util.LinkedList;


import model.ents.Entity;
import model.ents.Sun;
import model.input.SelectionBox;

public class GameHandler {
	private LinkedList<Entity> entities = new LinkedList<Entity>();
	public static int numEntities = 0;
	
	public GameHandler() {
	}
	
	/*
	 * mostly used for debug (?) to get all entities of a specified class (class being the String getClassName)
	 * i e all units, suns etc
	 */
	public LinkedList<Entity> getEntitiesByClass(String className) {
		LinkedList<Entity> foundEntities = new LinkedList<Entity>();
		if (entities.size() < 1)
			return foundEntities;
		for (int i = 0; i < entities.size(); i++) {
			Entity tempEnt = entities.get(i);
			if (tempEnt.getClassName().equals(className))
				foundEntities.add(tempEnt);
		}
		return foundEntities;
	}
	
	
	public void tick() { 
		for(int i = 0; i < entities.size(); i++) {
			entities.get(i).tick();
		}
		SelectionBox.tick();
	}
	
	public LinkedList<Entity> getEntities() {
		return this.entities;
	}
	
	public void killEntity(Entity ent) {
		this.removeObject(ent);
		ent = null;
	}
	
	// adds an entity to the handler for game handling
	// effectively provides tick() and render() for said entity
	public void addObject(Entity object) {
		if (this.entities.add(object)) {
			if (object.getClassName().equals("entity_sun"))
				WinCondition.addToTracking((Sun) object);
			numEntities++;
			//System.out.println("new " + object.getClassName());
		}
	}
	
	// removes entity from being handled by the game
	public void removeObject(Entity object) {
		if (this.entities.remove(object)) {
			numEntities--;
			//System.out.println("killed " + object.getClassName());
		}
	}
}
