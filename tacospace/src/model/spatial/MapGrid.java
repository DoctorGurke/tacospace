package model.spatial;

import java.util.LinkedList;

import model.Game;
import model.ents.Entity;

public class MapGrid {
	
	/*
	 * Get ready for spatial partitioning, yo! Fancy tech in game optimization when it comes to find out what is close to what
	 */
	
	public static final int MAP_GRIDSIZE = 32;
	private static Cell[][] cells = new Cell[Game.MAX_MAP_WIDTH / MAP_GRIDSIZE][Game.MAX_MAP_HEIGHT / MAP_GRIDSIZE];
	
	public static void setCell(Cell cell) {
		int x = cell.getIndexX();
		int y = cell.getIndexY();
		MapGrid.cells[(x < Game.MAX_MAP_WIDTH / MAP_GRIDSIZE - 1 && x > 0) ? x : 0][(y < Game.MAX_MAP_HEIGHT / MAP_GRIDSIZE - 1 && y > 0) ? y : 0] = cell;
	}
	
	public static Cell getCell(int indexX, int indexY) {
		if (cells[indexX][indexY] == null) // cell doesn't exist at that index yet, create a new one
			cells[indexX][indexY] = new Cell(indexX, indexY, true);
		return MapGrid.cells[indexX][indexY];
	}
	
	public static void clearGrid() {
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				cells[i][j] = null;
			}
		}
	}
	
	
	/*
	 * I FUCKING HATE THIS AAAAAH
	 */
	
	// includes original cell
	public static LinkedList<Entity> getNeighbours(int indexX, int indexY) {
		// regular middle cell
		LinkedList<Entity> entities = new LinkedList<Entity>();
		if (indexX >= 1 && indexX < Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY >= 1 && indexY < Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
			//return cells;
		// left side
		} else if (indexX < 1 && indexY >= 1 && indexY < Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = 0; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// right side
		} else if (indexX >= Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY >= 1 && indexY < Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = -1; x <= 0; x++) {
				for (int y = -1; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// top side
		} else if (indexX >= 1 && indexX < Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY < 1) {
			for (int x = -1; x <= 1; x++) {
				for (int y = 0; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// bottom side
		} else if (indexX >= 1 && indexX < Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY >= Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 0; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// top left corner
		} else if (indexX < 1 && indexY < 1) {
			for (int x = 0; x <= 1; x++) {
				for (int y = 0; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// bottom left corner
		} else if (indexX < 1 && indexY >= Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = 0; x <= 1; x++) {
				for (int y = -1; y <= 0; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// top right corner
		} else if (indexX >= Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY < 1) {
			for (int x = -1; x <= 0; x++) {
				for (int y = 0; y <= 1; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		// bottom right corner
		} else if (indexX >= Game.MAX_MAP_WIDTH / MapGrid.MAP_GRIDSIZE - 1 && indexY >= Game.MAX_MAP_HEIGHT / MapGrid.MAP_GRIDSIZE - 1) {
			for (int x = -1; x <= 0; x++) {
				for (int y = -1; y <= 0; y++) {
					entities.addAll(MapGrid.getCell(indexX + x, indexY + y).getContent());
				}
			}
			return entities;
		}
		return entities;
	}
}
