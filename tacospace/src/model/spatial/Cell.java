package model.spatial;

import java.util.LinkedList;

import model.ents.Entity;

public class Cell {
	private int indexX, indexY;
	private LinkedList<Entity> content = new LinkedList<Entity>();
	
	public Cell(int posX, int posY) {
		this.indexX = posX / MapGrid.MAP_GRIDSIZE;
		this.indexY = posY / MapGrid.MAP_GRIDSIZE;
		MapGrid.setCell(this);
	}
	
	public Cell(int indexX, int indexY, boolean index) {
		this.indexX = indexX;
		this.indexY = indexY;
		MapGrid.setCell(this);
	}

	public LinkedList<Entity> getUnits() {
		return this.content;
	}
	
	public void add(Entity entity) {
		this.content.add(entity);
		MapGrid.setCell(this);
	}
	
	public void remove(Entity entity) {
		this.content.remove(entity);
		MapGrid.setCell(this);
	}
	
	public LinkedList<Entity> getContent() {
		return this.content;
	}
	
	public int getIndexX() {
		return indexX;
	}

	public void setIndexX(int indexX) {
		this.indexX = indexX;
	}

	public int getIndexY() {
		return indexY;
	}

	public void setIndexY(int indexY) {
		this.indexY = indexY;
	}
	
}
