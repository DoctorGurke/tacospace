package model.input;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;

import model.Game;
import model.GameHandler;
import model.ents.*;
import view.Window;

public class KeyInput extends KeyAdapter {

	private GameHandler handler = Game.gameHandler;
	private boolean upPressed = false;
	private boolean downPressed = false;
	private boolean leftPressed = false;
	private boolean rightPressed = false;

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {

		case KeyEvent.VK_ESCAPE:
			Game.toggleGameMenu();
			break;

			/*
		// debug, kills selected units
		case KeyEvent.VK_X:
			// mutable object, not a copy
			LinkedList<Unit> units = SelectionBox.getSelectedUnits();
			while (units.size() > 0) {
				Unit tempUnit = units.get(0);
				handler.killEntity(tempUnit);
				SelectionBox.unselectUnit(tempUnit);
				tempUnit = null;
			}
			break;

*/
		// pauses running game without menu
		case KeyEvent.VK_SPACE:
			Game.toggleRunningGame();
			break;

		// camera movement
		case KeyEvent.VK_W:
			Window.getCamera().setAccY(1);
			upPressed = true;
			break;

		case KeyEvent.VK_S:
			Window.getCamera().setAccY(-1);
			downPressed = true;
			break;
			
		case KeyEvent.VK_A:
			Window.getCamera().setAccX(1);
			leftPressed = true;
			break;
			
		case KeyEvent.VK_D:
			Window.getCamera().setAccX(-1);
			rightPressed = true;
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		switch(key) {
		
		// camera movement
		case KeyEvent.VK_W:
			upPressed = false;
			if (downPressed) {
				Window.getCamera().setAccY(-1);
			} else {
				Window.getCamera().setAccY(0);
			}
			break;
			
		case KeyEvent.VK_S:
			downPressed = false;
			if (upPressed) {
				Window.getCamera().setAccY(1);
			} else {
				Window.getCamera().setAccY(0);
			}
			break;
			
		case KeyEvent.VK_A:
			leftPressed = false;
			if (rightPressed) {
				Window.getCamera().setAccX(-1);
			} else {
				Window.getCamera().setAccX(0);
			}
			break;
			
		case KeyEvent.VK_D: 
			rightPressed = false;
			if (leftPressed) {
				Window.getCamera().setAccX(1);
			} else {
				Window.getCamera().setAccX(0);
			}
			break;
		}
	}
}
