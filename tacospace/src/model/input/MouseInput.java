package model.input;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedList;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;

import model.Game;
import model.GameHandler;
import model.Player;
import model.ents.Unit;
import model.gui.GUIElement;
import model.gui.GUIHandler;
import model.util.Point;
import model.util.Rectangle;
import view.Window;

public class MouseInput extends MouseAdapter {

	private GameHandler handler = Game.gameHandler;
	private GUIHandler guihandler = Game.guiHandler;

	Clip audioClip;

	public MouseInput() {

		// prepare sound for menu click bleep
		try {
			AudioInputStream audioStream = AudioSystem
					.getAudioInputStream(new File(getClass().getResource("/resource/sound/main_menu_2.wav").toURI()));
			AudioFormat format = audioStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, format);
			audioClip = (Clip) AudioSystem.getLine(info);
			audioClip.open(audioStream);
			FloatControl gainControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
			float volume = 0.5f; // TODO: change via settings menu
			float range = gainControl.getMaximum() - gainControl.getMinimum();
			float gain = (range * volume) + gainControl.getMinimum();
			gainControl.setValue(gain);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void mousePressed(MouseEvent e) {
		int key = e.getButton();

		switch (key) {

		case MouseEvent.BUTTON1:
			if (Game.gameRunning() || Game.gamePaused()) { 
				// game is active. Either running or paused. anything but menu state etc
				
				// start a new selection
				SelectionBox.clear(); // clears currently selected units
				SelectionBox.reset(); // resets SelectionBox positions etc
				if (!SelectionBox.isSelecting()) {
					SelectionBox.setX(e.getX());
					SelectionBox.setY(e.getY());
					SelectionBox.setEndX(e.getX());
					SelectionBox.setEndY(e.getY());
					SelectionBox.setPreSelecting(true);
				}
			} else if (Game.menuState()) { // menu state
				// menu clicking
				LinkedList<GUIElement> elements = guihandler.getElements();
				for (int i = 0; i < elements.size(); i++) {
					GUIElement element = elements.get(i);
					Rectangle elementBounds = new Rectangle(element.getX(), element.getY(), element.getX() + element.getWidth(), element.getY() + element.getHeight());
					if (elementBounds.contains(e.getX(), e.getY()) && Game.getGameState() == element.getGameState()) {
						audioClip.setMicrosecondPosition(0);
						// audioClip.stop();
						audioClip.start();

						// master switch for button action
						switch (element.getAction()) {
						case GUIElement.ACTION_NOACTION:
							System.out.println("no action");
							break;

						case GUIElement.ACTION_EXIT:
							System.out.println("action exit");
							Game.stop();
							break;

						case GUIElement.ACTION_START_GAME:
							System.out.println("action start");
							Game.startGame();
							break;

						case GUIElement.ACTION_TOGGLE_GAME:
							System.out.println("action toggle");
							Game.toggleRunningGame();
							break;

						case GUIElement.ACTION_MAIN_MENU:
							System.out.println("action menu");
							Game.setGameState(Game.GAME_STATE_MAIN_MENU);
							Game.resetGame();
							break;

						case GUIElement.ACTION_LOGIN:
							System.out.println("action login");
							break;

						case GUIElement.ACTION_LOGIN_MENU:
							System.out.println("action login menu");
							break;

						case GUIElement.ACTION_SETTINGS_MENU:
							System.out.println("action settings menu");
							break;

						case GUIElement.ACTION_COLOR_SELECT_MENU:
							System.out.println("action color select menu");
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						case GUIElement.ACTION_MAP_SELECT_MENU:
							System.out.println("action map select menu");
							Game.setGameState(Game.GAME_STATE_MAP_SELECT);
							break;

						/*
						 * Map selection options
						 */

						case GUIElement.ACTION_MAP_1:
							System.out.println("action map 1");
							Game.setActiveMap(1);
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						case GUIElement.ACTION_MAP_2:
							System.out.println("action map 2");
							Game.setActiveMap(2);
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						case GUIElement.ACTION_MAP_3:
							System.out.println("action map 3");
							Game.setActiveMap(3);
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						case GUIElement.ACTION_MAP_4:
							System.out.println("action map 4");
							Game.setActiveMap(4);
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						case GUIElement.ACTION_MAP_5:
							System.out.println("action map 5");
							Game.setActiveMap(5);
							Game.setGameState(Game.GAME_STATE_COLOUR_SELECT);
							break;

						/*
						 * Color selection options
						 */

						case GUIElement.ACTION_COLOR_GREEN:
							System.out.println("action color green");
							Player.setTeam(Game.getTeamByName("green"));
							Game.startGame();
							break;

						case GUIElement.ACTION_COLOR_BLUE:
							System.out.println("action color blue");
							Player.setTeam(Game.getTeamByName("blue"));
							Game.startGame();
							break;

						case GUIElement.ACTION_COLOR_MAGENTA:
							System.out.println("action color magenta");
							Player.setTeam(Game.getTeamByName("magenta"));
							Game.startGame();
							break;

						case GUIElement.ACTION_COLOR_PURPLE:
							System.out.println("action color purple");
							Player.setTeam(Game.getTeamByName("purple"));
							Game.startGame();
							break;

						case GUIElement.ACTION_COLOR_ORANGE:
							System.out.println("action color orange");
							Player.setTeam(Game.getTeamByName("orange"));
							Game.startGame();
							break;

						}
					}
				}
			}
			break;
/*
		// DEBUG: spawns a unit of the player's team at mouse position
		// TODO: REMOVE THIS BEFORE SUBMITTING
		case MouseEvent.BUTTON2:
			if (Game.gameRunning()) {
				this.handler.addObject(new Unit(e.getX() - Window.getCamera().getOffsetX(), e.getY() - Window.getCamera().getOffsetY(), Player.getTeam()));
			}
			break;

*/
		// sends selected units to mouse position by setting move goal
		case MouseEvent.BUTTON3:
			if (!Game.menuState()) {
				LinkedList<Unit> units = SelectionBox.getSelectedUnits();
				for (int i = 0; i < units.size(); i++) {
					units.get(i).setMoveGoal(new Point(e.getX() - Window.getCamera().getOffsetX(), e.getY() - Window.getCamera().getOffsetY()));
				}
			}
			break;
		}
	}

	// stops selection box
	public void mouseReleased(MouseEvent e) {
		int key = e.getButton();
		
		switch(key) {
		
		case MouseEvent.BUTTON1:
			if (SelectionBox.isSelecting()) {
				SelectionBox.setPreSelecting(false);
				SelectionBox.setSelecting(false);
				SelectionBox.reset();
			}
			break;
		}
	}
}
