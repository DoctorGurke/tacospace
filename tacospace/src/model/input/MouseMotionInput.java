package model.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.LinkedList;

import model.Game;
import model.gui.GUIElement;
import model.gui.GUIHandler;
import model.util.Rectangle;

public class MouseMotionInput extends MouseMotionAdapter {
	
	private GUIHandler guihandler = Game.guiHandler;
			
	public void mouseDragged(MouseEvent e) {
		// selection box updating
		if (Game.gameRunning() || Game.gamePaused()) {
			if (SelectionBox.isPreSelecting()) {
				SelectionBox.setSelecting(true);
				SelectionBox.setEndX(e.getX());
				SelectionBox.setEndY(e.getY());
			}
		}
	}
	
	// gui elements hovering
	public void mouseMoved(MouseEvent e) {
		if (Game.menuState()) {
			LinkedList<GUIElement> elements = guihandler.getElements();
			for (int i = 0; i < elements.size(); i++) {
				GUIElement element = elements.get(i);
				Rectangle elementBounds = new Rectangle(element.getX(), element.getY(), element.getX() + element.getWidth(), element.getY() + element.getHeight());
				if (elementBounds.contains(e.getX(), e.getY())) {
					element.setHovered(true);
				} else {
					element.setHovered(false);
				}
			}
		}
	}
}
