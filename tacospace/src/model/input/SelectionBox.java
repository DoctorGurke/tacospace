package model.input;

import java.util.LinkedList;

import model.Game;
import model.Player;
import model.ents.*;
import model.util.Rectangle;
import view.Window;

public class SelectionBox {
	
	/*
	 * entity to represent and use the selection box drawn by player mouse input
	 */
	
	private static int x, y, endX, endY = 0;
	private static boolean preSelecting, selecting = false;
	private static LinkedList<Unit> selectedUnits = new LinkedList<Unit>();

	public SelectionBox(int x, int y) {
		SelectionBox.x = x;
		SelectionBox.y = y;
	}
	
	public static void tick() {
		if (SelectionBox.isSelecting()) {
			LinkedList<Entity> units =  Game.gameHandler.getEntitiesByClass("entity_unit");
			for (int i = 0; i < units.size(); i++) {
				Entity tempUnit = units.get(i);
				if (new Rectangle(SelectionBox.getX(), SelectionBox.getY(), SelectionBox.getEndX(), SelectionBox.getEndY()).contains(tempUnit.getPosX() + Window.getCamera().getOffsetX(), tempUnit.getPosY() + Window.getCamera().getOffsetY())) {
					if ( !((Unit) tempUnit).isSelected() && ((Unit) tempUnit).getTeam() == Player.getTeam() ) {
						((Unit) tempUnit).setSelected(true);
						selectedUnits.add(((Unit) tempUnit));
					}
				} else {
					((Unit) tempUnit).setSelected(false);
					selectedUnits.remove(((Unit) tempUnit));
				}
			}
		}
	}
	
	public static void clear() {
		LinkedList<Entity> units =  Game.gameHandler.getEntitiesByClass("entity_unit");
		for (int i = 0; i < units.size(); i++) {
			Unit unit = (Unit) units.get(i);
			unit.setSelected(false);
			selectedUnits.remove(unit);
		}
	}
	
	public static void reset() {
		SelectionBox.setX(0);
		SelectionBox.setY(0);
		SelectionBox.setEndX(0);
		SelectionBox.setEndY(0);
	}
	
	public static void unselectUnit(Unit unit) {
		unit.setSelected(false);
		selectedUnits.remove(unit);
	}
	
	public static LinkedList<Unit> getSelectedUnits() {
		return selectedUnits;
	}

	public static boolean isPreSelecting() {
		return preSelecting;
	}

	public static void setPreSelecting(boolean preSelecting) {
		SelectionBox.preSelecting = preSelecting;
	}
	
	public static boolean isSelecting() {
		return SelectionBox.selecting;
	}
	
	public static void setSelecting(boolean selecting) {
		SelectionBox.selecting = selecting;
	}

	public static int getX() {
		return SelectionBox.x;
	}

	public static void setX(int x) {
		SelectionBox.x = x;
	}

	public static int getY() {
		return SelectionBox.y;
	}

	public static void setY(int y) {
		SelectionBox.y = y;
	}

	public static void setEndX(int endX) {
		SelectionBox.endX = endX;
	}
	
	public static void setEndY(int endY) {
		SelectionBox.endY = endY;
	}
	
	public static int getEndX() {
		return SelectionBox.endX;
	}
	
	public static int getEndY() {
		return SelectionBox.endY;
	}

}
