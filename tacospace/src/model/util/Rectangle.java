package model.util;

public class Rectangle {
	private int startX, startY, endX, endY;
	
	public Rectangle(int startX, int startY, int endX, int endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}
	
	/*
	 * I hate everything about this method
	 * used to see if a point is within a rectangle
	 */
	public boolean contains(double x, double y) {
		if(this.endX > this.startX && this.endY > this.startY) {
			return ((x > this.startX && x < this.endX) && (y > this.startY && y < this.endY));
			//return normalizedContains(x, y, this.startX, this.startY, this.endX, this.endY);
		} else if (this.endX < this.startX && this.endY < this.startY) {
			return ((x < this.startX && x > this.endX) && (y < this.startY && y > this.endY));
			//return normalizedContains(x, y, this.endX, this.endY, this.startX, this.startY);
		} else if (this.endX > this.startX && this.endY < this.startY) {
			return ((x > this.startX && x < this.endX) && (y < this.startY && y > this.endY));
			//return normalizedContains(x, y, this.startX, this.endY, this.endX, this.startY);
		} else if (this.endX < this.startX && this.endY > this.startY) {
			return ((x < this.startX && x > this.endX) && (y > this.startY && y < this.endY));
			//return normalizedContains(x, y, this.endX, this.startY, this.startX, this.endY);
		}
		return false;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public int getEndX() {
		return endX;
	}

	public void setEndX(int endX) {
		this.endX = endX;
	}

	public int getEndY() {
		return endY;
	}

	public void setEndY(int endY) {
		this.endY = endY;
	}
	
	
}
