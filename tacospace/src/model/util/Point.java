package model.util;

public class Point {
	
	private double x, y;
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return (int) this.x;
	}
	
	public double getPosX() {
		return this.x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public int getY() {
		return (int) this.y;
	}
	
	public double getPosY() {
		return this.y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double getDistance(Point point) {
		return Math.sqrt(Math.pow((this.getX() - point.getX()), 2) + Math.pow((this.getY() - point.getY()), 2));
	}
	
	public boolean isSamePosition(Point point) {
		return (this.getX() == point.getX() && this.getY() == point.getY()) ? true : false;
	}
	
}
