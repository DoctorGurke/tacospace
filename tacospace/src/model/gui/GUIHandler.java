package model.gui;

import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class GUIHandler {
    
    private LinkedList<GUIElement> elements = new LinkedList<GUIElement>();
 
    public LinkedList<GUIElement> getElements() {
        return this.elements;
    }
    
    // adds an element to the gui handler for gui handling
    public void addObject(GUIElement element) {
        this.elements.add(element);
    }
}