package model.gui;

import java.awt.Color;

public class GUIElement {
	
	private int action;
	
	// placeholder
	public static final int ACTION_NOACTION = 0;
	
	// menu buttons
	public static final int ACTION_EXIT = 1; // closes game
	public static final int ACTION_START_GAME = 2; // starts a game with settings, default settings apply
	public static final int ACTION_TOGGLE_GAME = 3; // pause/resume, same as spacebar
	public static final int ACTION_MAIN_MENU = 4; // returns to main menu
	public static final int ACTION_LOGIN = 5; // login button after credentials are entered
	public static final int ACTION_LOGIN_MENU = 6; // back to login, basically a log out
	public static final int ACTION_SETTINGS_MENU = 7; // opens settings menu
	public static final int ACTION_COLOR_SELECT_MENU = 10; // to colour selection
	public static final int ACTION_MAP_SELECT_MENU = 11; // to map selection
	
	// map select buttons
	public static final int ACTION_MAP_1 = 101;
	public static final int ACTION_MAP_2 = 102;
	public static final int ACTION_MAP_3 = 103;
	public static final int ACTION_MAP_4 = 104;
	public static final int ACTION_MAP_5 = 105;
	
	// colour select buttons
	public static final int ACTION_COLOR_RED = 201;
	public static final int ACTION_COLOR_GREEN = 202;
	public static final int ACTION_COLOR_MAGENTA = 203;
	public static final int ACTION_COLOR_PURPLE = 204;
	public static final int ACTION_COLOR_YELLOW = 205;
	public static final int ACTION_COLOR_ORANGE = 206;
	public static final int ACTION_COLOR_BLUE = 207;
	//public static final int ACTION_
	
	private int x, y, width, height, gameState;
	private String title;
	private boolean underlined, hovered, clicked, coloured;
	private Color color, hovercolor;
	// image?
	
	// gui element with underlined title
	public GUIElement(int gameState, int action, int x, int y, int width, int height, String title, boolean underlined) {
		this.gameState = gameState;
		this.action = action;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.title = title;
		this.setUnderlined(underlined);
	}
	
	// gui element with specific colour
	// mainly used for colour selection
	public GUIElement(int gameState, int action, int x, int y, int width, int height, String title, Color color, Color hovercolor) {
		this.gameState = gameState;
		this.action = action;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.title = title;
		this.setColoured(true);
		this.setColor(color);
		this.setHovercolor(hovercolor);
	}
	
	
	// regular gui element
	public GUIElement(int gameState, int action, int x, int y, int width, int height, String title) {
		this.gameState = gameState;
		this.action = action;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.title = title;
	}

	public int getGameState() {
		return gameState;
	}

	public int getAction() {
		return this.action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public boolean isHovered() {
		return hovered;
	}

	public void setHovered(boolean hovered) {
		this.hovered = hovered;
	}

	public boolean isClicked() {
		return clicked;
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isColoured() {
		return coloured;
	}

	public void setColoured(boolean coloured) {
		this.coloured = coloured;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getHovercolor() {
		return hovercolor;
	}

	public void setHovercolor(Color hovercolor) {
		this.hovercolor = hovercolor;
	}

	public boolean isUnderlined() {
		return underlined;
	}

	public void setUnderlined(boolean underlined) {
		this.underlined = underlined;
	}
	
	
}
