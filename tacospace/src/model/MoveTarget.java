package model;

import model.util.Point;

public class MoveTarget {
	
	/*
	 * move target class for movement handling
	 */

	// using Point since this will be used like a vector alot
	private Point pos;
	
	public MoveTarget(Point point) {
		if (point.getX() < 0)
			point.setX(0);
		else if (point.getX() > Game.MAX_MAP_WIDTH)
			point.setX(Game.MAX_MAP_WIDTH);
		if (point.getY() < 0)
			point.setY(0);
		else if (point.getY() > Game.MAX_MAP_HEIGHT)
			point.setY(Game.MAX_MAP_HEIGHT);
		this.pos = new Point(point.getPosX(), point.getPosY());
	}
	
	public MoveTarget(double x, double y) {
		if (x < 0)
			x = 0;
		else if (x > Game.MAX_MAP_WIDTH)
			x = Game.MAX_MAP_WIDTH;
		if (y < 0)
			y = 0;
		else if (y > Game.MAX_MAP_HEIGHT)
			y = Game.MAX_MAP_HEIGHT;
		this.pos = new Point(x, y);
	}
	
	public MoveTarget(int x, int y) {
		if (x < 0)
			x = 0;
		else if (x > Game.MAX_MAP_WIDTH)
			x = Game.MAX_MAP_WIDTH;
		if (y < 0)
			y = 0;
		else if (y > Game.MAX_MAP_HEIGHT)
			y = Game.MAX_MAP_HEIGHT;
		this.pos = new Point(x, y);
	}
	
	public Point getPos() {
		return this.pos;
	}
	
	public void setPos(Point point) {
		if (point.getX() < 0)
			point.setX(0);
		else if (point.getX() > Game.MAX_MAP_WIDTH)
			point.setX(Game.MAX_MAP_WIDTH);
		if (point.getY() < 0)
			point.setY(0);
		else if (point.getY() > Game.MAX_MAP_HEIGHT)
			point.setY(Game.MAX_MAP_HEIGHT);
		this.pos = point;
	}

	public double getX() {
		return this.pos.getPosX();
	}

	public void setX(double x) {
		if (x < 0)
			x = 0;
		else if (x > Game.MAX_MAP_WIDTH)
			x = Game.MAX_MAP_WIDTH;
		this.pos.setX(x);
	}

	public double getY() {
		return this.pos.getPosY();
	}

	public void setY(double y) {
		if (y < 0)
			y = 0;
		else if (y > Game.MAX_MAP_HEIGHT)
			y = Game.MAX_MAP_HEIGHT;
		this.pos.setX(y);
	}

}
